<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Home</title>
	</head>
	<body>
	<%
		String name = session.getAttribute("name").toString();
		String type = session.getAttribute("type").toString();
		String message = "";
		
		if(type.equals("applicant")) {
			message = "Welcome applicant. You may now start looking for your career opportunity.";
		} else {
			message = "Welcome employer. You may now start browsing applicant profiles.";
		}
	%>
		<h1>Welcome <%= name %>!</h1>
		<p><%= message %> </p>
	</body>
</html>